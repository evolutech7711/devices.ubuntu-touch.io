import {
  componentScriptLoader,
  onLoadAndNavigation
} from "@logic/client/clientNavigation.js";
import FlexSearch from "flexsearch";
import progressStages from "@data/progressStages.json";

export default function registerSearch() {
  // Run once
  componentScriptLoader("#sidebarCollapse", () => {
    // Regenerate search results
    function search(e) {
      // Get search term and fetch matching codenames
      const searchTerm = document.getElementById("searchInput").value;
      const isSearched = searchTerm.length >= 2;
      const searchRelease = getFilterValues(["release"])[0];
      let results = [];

      if (isSearched) {
        results = FSdoc.search({
          query: searchTerm,
          limit: 50,
          enrich: true
        });

        // Format data
        results = [].concat(...results.map((r) => r.result));
      } else {
        results = window.searchData;
      }

      let unfilteredCount = [...new Set(results)].filter(
        (r) => r.doc.release == searchRelease
      ).length;
      results = [...new Set(applyFilters(results))];
      displayResults(results, unfilteredCount);
    }

    // Sort based on given parameter
    function sort() {
      const sortVal = getFilterValues(["sortOrder"])[0];

      // Get list elements from DOM
      let listElement = document.getElementsByClassName("devices-list")[0];
      let deviceList = Object.values(listElement.children);
      let outList = deviceList.sort((a, b) => {
        if (!a.dataset[sortVal]) return 1;
        if (!b.dataset[sortVal]) return -1;
        return sortVal == "name"
          ? a.dataset[sortVal].localeCompare(b.dataset[sortVal])
          : parseInt(b.dataset[sortVal]) - parseInt(a.dataset[sortVal]);
      });
      while (listElement.lastChild) {
        listElement.removeChild(listElement.lastChild);
      }
      outList.forEach((el) => {
        listElement.appendChild(el);
      });
    }

    function displayResults(results, unfilteredCount) {
      // Get list elements from DOM
      let deviceList = Object.values(
        document.getElementsByClassName("devices-list")[0].children
      );

      // Hide (display: none) all non-matching devices
      deviceList.forEach((device) => {
        let isShown = results.includes(device.dataset.id);
        if (!isShown) device.classList.add("d-none");
        else device.classList.remove("d-none");
      });

      setResultsCount(results.length, unfilteredCount);
    }

    // Apply search filters
    function applyFilters(results) {
      const filters = getFilterValues([
        "deviceType",
        "portType",
        "featureStage",
        "installerFilter",
        "waydroidFilter",
        "displayOutFilter",
        "release"
      ]);
      return results
        .filter((a) => {
          return (
            (!filters[0].length || filters[0].includes(a.doc.deviceType)) &&
            a.doc.progressStage >= parseInt(filters[2]) &&
            (!filters[1].length || filters[1].includes(a.doc.portType)) &&
            (filters[3] ? a.doc.installerAvailable : true) &&
            (filters[4] ? a.doc.waydroidAvailable : true) &&
            (filters[5] ? a.doc.displayOutAvailable : true) &&
            filters[6] == a.doc.release
          );
        })
        .map((a) => a.id);
    }

    // Get filter values
    function getFilterValues(query) {
      return query.map((elName) => {
        let elArray = Object.values(
          document.querySelectorAll("[name='" + elName + "']")
        );
        let filterVal = elArray.filter((e) => e.checked).map((e) => e.value);
        filterVal =
          elArray[0].type == "radio"
            ? filterVal[0]
            : elArray.length == 1
            ? filterVal.length == 1
            : filterVal;
        return filterVal;
      });
    }

    function disableFilters() {
      [
        "deviceType",
        "portType",
        "installerFilter",
        "waydroidFilter",
        "displayOutFilter"
      ].forEach((elName) => {
        document
          .querySelectorAll("[name='" + elName + "']")
          .forEach((el) => (el.checked = false));
      });
      let featureStageSelector = document.querySelector(
        "[name='featureStage'][value='0']"
      );
      featureStageSelector.checked = true;
      featureStageSelector.dispatchEvent(new Event("change"));
      search();
    }

    // Set search results count in UI
    function setResultsCount(count, unfilteredCount) {
      let deviceSearchCount = document.getElementById("deviceSearchCount");
      let searchNoResults = document.getElementById("searchNoResults");
      let filteredWarning = document.getElementById("filteredWarning");
      let deviceSearchMobileCount =
        document.getElementById("deviceCountMobile");

      if (unfilteredCount == 0) {
        searchNoResults.classList.remove("d-none");
        filteredWarning.classList.add("d-none");
      } else if (unfilteredCount > count) {
        searchNoResults.classList.add("d-none");
        filteredWarning.classList.remove("d-none");
      } else {
        searchNoResults.classList.add("d-none");
        filteredWarning.classList.add("d-none");
      }

      deviceSearchCount.innerText =
        count == 1
          ? "One device"
          : count == 0
          ? "No devices"
          : count + " devices";
      deviceSearchMobileCount.innerText =
        count == 1
          ? "One supported device"
          : count == 0
          ? "No supported devices"
          : count + " supported devices";
    }

    // Setup flexsearch
    var FSdoc = new FlexSearch.Document({
      profile: "default",
      tokenize: "forward",
      minlength: 2,
      document: {
        id: "id",
        index: ["codename", "name"],
        store: ["deviceType", "portType", "progressStage", "installerAvailable"]
      }
    });

    // Import data from json to flexsearch
    fetch("/search-index.json")
      .then((response) => response.json())
      .then((data) => {
        data.forEach((fsearchExport) => {
          FSdoc.import(fsearchExport.key, fsearchExport.data);
          if (fsearchExport.key == "store") {
            let searchData = JSON.parse(fsearchExport.data);
            window.searchData = Object.entries(searchData).map((d) => {
              return { id: d[0], doc: d[1] };
            });
          }
        });

        // Register events
        document.getElementById("searchInput").onkeyup = search;
        document.querySelectorAll("input[name]").forEach((el) => {
          el.onchange = el.name == "sortOrder" ? sort : search;
        });
        document.getElementById("disableFilters").onclick = disableFilters;

        sort();
        search();
      });

    document.querySelectorAll(".toggle-sidebarCollapse").forEach((el) => {
      el.onclick = (ev) => {
        document.querySelector("#sidebarCollapse").classList.toggle("show");
      };
    });

    document.querySelectorAll(".advanced-filter-toggle").forEach((el) => {
      el.onclick = (ev) => {
        document.querySelector("#advancedFilters").classList.toggle("show");
      };
    });

    document.querySelectorAll(".advanced-filter-show").forEach((el) => {
      el.onclick = (ev) => {
        document.querySelector("#advancedFilters").classList.add("show");
      };
    });
  });

  // Always run
  onLoadAndNavigation(() => {
    // Hide sidebar on page load on mobile
    document.querySelector("#sidebarCollapse")?.classList.remove("show");
    // Mark current device
    document.querySelector(".active--exact")?.classList.remove("active--exact");
    document
      .querySelector(
        "#sidebarCollapse [href='" +
          window.location.pathname.replace(/\/+$/, "") +
          "']"
      )
      ?.classList.add("active--exact");
  });
}
